import Main from '@/components/main'
import parentView from '@/components/parent-view'
import mult from './multilevel'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */

export default [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: () => import('@/view/login/login.vue')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          hideInMenu: true,
          title: '首页',
          notCache: true,
          icon: 'md-home'
        },
        component: () => import('@/view/single-page/home')
      }
    ]
  },
  /* {
    path: '/tools_methods',
    name: 'tools_methods',
    meta: {
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'tools_methods_page',
        name: 'tools_methods_page',
        meta: {
          icon: 'ios-hammer',
          title: '工具方法',
          beforeCloseName: 'before_close_normal'
        },
        component: () => import('@/view/tools-methods/tools-methods.vue')
      }
    ]
  }, */
  {
    path: '/admin',
    name: 'admin',
    meta: {
      icon: 'md-menu',
      title: '系统管理'
    },
    component: Main,
    children: [
      {
        path: 'user-manage',
        name: 'user-manage',
        meta: {
          access: ['user:manage'],
          icon: 'md-funnel',
          title: '用户管理'
        },
        component: () => import('@/view/admin/user-manage.vue')
      },
      {
        path: 'role-manage',
        name: 'role-manage',
        meta: {
          access: ['role:manage'],
          icon: 'md-funnel',
          title: '角色管理'
        },
        component: () => import('@/view/admin/role-manage.vue')
      },
      {
        path: 'group-manage',
        name: 'group-manage',
        meta: {
          access: ['group:manage'],
          icon: 'md-funnel',
          title: '用户组管理'
        },
        component: () => import('@/view/admin/group-manage.vue')
      },
      {
        path: 'power-manage',
        name: 'power-manage',
        meta: {
          access: ['power:manage'],
          icon: 'md-funnel',
          title: '权限管理'
        },
        component: () => import('@/view/admin/power-manage.vue')
      },
      {
        path: 'menu-manage',
        name: 'menu-manage',
        meta: {
          access: ['menu:manage'],
          icon: 'md-funnel',
          title: '菜单管理'
        },
        component: () => import('@/view/admin/menu-manage.vue')
      },
      {
        path: 'module-manage',
        name: 'module-manage',
        meta: {
          access: ['module:manage'],
          icon: 'md-funnel',
          title: '模块管理'
        },
        component: () => import('@/view/admin/module-manage.vue')
      },
      {
        path: '',
        name: 'swagger',
        meta: {
          access: ['swagger:manage'],
          title: 'swagger文档',
          href: 'http://192.168.190.132:9001/swagger-ui.html',
          // href: 'http://api-gateway-svc:9001/swagger-ui.html',
          icon: 'ios-book'
        }
      },
      {
        path: '',
        name: 'admin-client',
        meta: {
          access: ['admin:manage'],
          title: 'admin监控',
          href: 'http://192.168.190.132:8764',
          // href: 'http://monitor-svc:8764',
          icon: 'ios-book'
        }
      },
      {
        path: '',
        name: 'druid',
        meta: {
          access: ['druid:manage'],
          title: 'druid监控',
          href: 'http://192.168.190.132:9001/admin-service/druid/login.html',
          // href: 'http://api-gateway-svc:9001/admin-service/druid/login.html',
          icon: 'ios-book'
        }
      },
      {
        path: '',
        name: 'nacos',
        meta: {
          access: ['nacos:manage'],
          title: 'nacos注册中心',
          href: 'http://192.168.190.132:8848/nacos/index.html',
          icon: 'ios-book'
        }
      },
      /*{
        path: 'oper-manage',
        name: 'oper-manage',
        meta: {
          icon: 'md-funnel',
          title: '操作管理'
        },
        component: () => import('@/view/admin/oper-manage.vue')
      },
      {
        path: 'menu-manage',
        name: 'menu-manage',
        meta: {
          icon: 'md-funnel',
          title: '菜单管理'
        },
        component: () => import('@/view/admin/menu-manage.vue')
      },
      {
        path: 'dict-manage',
        name: 'dict-manage',
        meta: {
          icon: 'md-funnel',
          title: '字典管理'
        },
        component: () => import('@/view/admin/dict-manage.vue')
      },
      {
        path: 'config-manage',
        name: 'config-manage',
        meta: {
          icon: 'md-funnel',
          title: '配置管理'
        },
        component: () => import('@/view/admin/config-manage.vue')
      }*/
    ]
  },

  // 多级菜单
  // mult,

  {
    path: '/argu',
    name: 'argu',
    meta: {
      hideInMenu: true
    },
    component: Main,
    children: [
      {
        path: 'params/:id',
        name: 'params',
        meta: {
          icon: 'md-flower',
          title: route => `{{ params }}-${route.params.id}`,
          notCache: true,
          beforeCloseName: 'before_close_normal'
        },
        component: () => import('@/view/argu-page/params.vue')
      },
      {
        path: 'query',
        name: 'query',
        meta: {
          icon: 'md-flower',
          title: route => `{{ query }}-${route.query.id}`,
          notCache: true
        },
        component: () => import('@/view/argu-page/query.vue')
      }
    ]
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  }
]
