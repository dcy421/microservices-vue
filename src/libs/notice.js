import {Notice} from 'view-design'

const ADD_SUCCESS_NOTICE = '添加成功';
const UPDATE_SUCCESS_NOTICE = '修改成功';
const DELETE_SUCCESS_NOTICE = '删除成功';
const SAVE_SUCCESS_NOTICE = '保存成功';
const ADD_ERROR_NOTICE = '添加失败';
const UPDATE_ERROR_NOTICE = '修改失败';
const DELETE_ERROR_NOTICE = '删除失败';
const SAVE_ERROR_NOTICE = '保存失败';
const NOTICE_DURATION = 3

/**
 * 获取通知配置
 * @param title
 * @param desc
 * @param duration
 * @returns {{duration: *, title: *, desc: *}}
 */
export const noticeConfig = (title, desc = null, duration = 3) => {
  return {title: title, desc: desc, duration: duration}
}

/**
 * 成功通知
 * @param type
 * @param desc
 */
export const noticeSuccess = (type, desc = null) => {
  switch (type) {
    case 'add':
      Notice.success({title: ADD_SUCCESS_NOTICE, desc: desc, duration: NOTICE_DURATION});
      break;
    case 'upd':
      Notice.success({title: UPDATE_SUCCESS_NOTICE, desc: desc, duration: NOTICE_DURATION});
      break;
    case 'del':
      Notice.success({title: DELETE_SUCCESS_NOTICE, desc: desc, duration: NOTICE_DURATION});
      break;
    default:
      Notice.success({title: SAVE_SUCCESS_NOTICE, desc: desc, duration: NOTICE_DURATION});
  }
}

/**
 * 失败通知
 * @param type
 * @param desc
 */
export const noticeError = (type, desc = null) => {
  switch (type) {
    case 'add':
      Notice.error({title: ADD_ERROR_NOTICE, desc: desc, duration: NOTICE_DURATION});
      break;
    case 'upd':
      Notice.error({title: UPDATE_ERROR_NOTICE, desc: desc, duration: NOTICE_DURATION});
      break;
    case 'del':
      Notice.error({title: DELETE_ERROR_NOTICE, desc: desc, duration: NOTICE_DURATION});
      break;
    default:
      Notice.error({title: SAVE_ERROR_NOTICE, desc: desc, duration: NOTICE_DURATION});
  }
}
