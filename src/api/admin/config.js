import axios from '@/libs/api.request'

/**
 * 添加用户
 * @param config
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const addConfig = (config) => {
  return axios.request({
    url: '/admin-service/config/save',
    method: 'post',
    data: config
  })
}

/**
 * 修改用户
 * @param config
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const updateConfig = (config) => {
  return axios.request({
    url: '/admin-service/config/update',
    method: 'post',
    data: config
  })
}

/**
 * 删除用户
 * @param configId
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteConfigById = (configId) => {
  return axios.request({
    url: '/admin-service/config/delete',
    method: 'post',
    params: {
      id: configId
    }
  })
}

/**
 * 批量删除用户
 * @param configIds
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteBatchConfigById = (configIds) => {
  return axios.request({
    url: '/admin-service/config/deleteBatch',
    method: 'post',
    data: configIds
  })
}
