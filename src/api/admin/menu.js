import axios from '@/libs/api.request'

/**
 * 添加用户
 * @param menu
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const addMenu = (menu) => {
  return axios.request({
    url: '/admin-service/menu/save',
    method: 'post',
    data: menu
  })
}

/**
 * 修改用户
 * @param menu
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const updateMenu = (menu) => {
  return axios.request({
    url: '/admin-service/menu/update',
    method: 'post',
    data: menu
  })
}

/**
 * 删除用户
 * @param menuId
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteMenuById = (menuId) => {
  return axios.request({
    url: '/admin-service/menu/delete',
    method: 'post',
    params: {
      id: menuId
    }
  })
}

/**
 * 批量删除用户
 * @param menuIds
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteBatchMenuById = (menuIds) => {
  return axios.request({
    url: '/admin-service/menu/deleteBatch',
    method: 'post',
    data: menuIds
  })
}

/**
 * 获取模块tree-table数据
 * @returns {*|AxiosPromise<any>|ClientRequest|void|ClientHttp2Stream}
 */
export const getMenuTreeTableList = () => {
  return axios.request({
    url: '/admin-service/menu/getMenuTreeTableList'
  })
}

/**
 * 获取tree列表数据
 * @returns {*|AxiosPromise<any>|ClientRequest|void|ClientHttp2Stream}
 */
export const getMenuTreeList = (powerId) => {
  return axios.request({
    url: '/admin-service/menu/getMenuTreeListByPowerId',
    params: {
      powerId: powerId
    }
  })
}
