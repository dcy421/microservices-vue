import axios from '@/libs/api.request'

/**
 * 添加用户
 * @param dict
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const addDict = (dict) => {
  return axios.request({
    url: '/admin-service/dict/save',
    method: 'post',
    data: dict
  })
}

/**
 * 修改用户
 * @param dict
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const updateDict = (dict) => {
  return axios.request({
    url: '/admin-service/dict/update',
    method: 'post',
    data: dict
  })
}

/**
 * 删除用户
 * @param dictId
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteDictById = (dictId) => {
  return axios.request({
    url: '/admin-service/dict/delete',
    method: 'post',
    params: {
      id: dictId
    }
  })
}

/**
 * 批量删除用户
 * @param dictIds
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteBatchDictById = (dictIds) => {
  return axios.request({
    url: '/admin-service/dict/deleteBatch',
    method: 'post',
    data: dictIds
  })
}
