import axios from '@/libs/api.request'

/**
 * 添加用户
 * @param role
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const addRole = (role) => {
  return axios.request({
    url: '/admin-service/role/save',
    method: 'post',
    data: role
  })
}

/**
 * 修改用户
 * @param role
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const updateRole = (role) => {
  return axios.request({
    url: '/admin-service/role/update',
    method: 'post',
    data: role
  })
}

/**
 * 删除用户
 * @param roleId
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteRoleById = (roleId) => {
  return axios.request({
    url: '/admin-service/role/delete',
    method: 'post',
    params: {
      id: roleId
    }
  })
}

/**
 * 批量删除用户
 * @param roleIds
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteBatchRoleById = (roleIds) => {
  return axios.request({
    url: '/admin-service/role/deleteBatch',
    method: 'post',
    data: roleIds
  })
}

/**
 * 获取角色列表
 * @returns {*|AxiosPromise<any>|ClientRequest|void|ClientHttp2Stream}
 */
export const getRoleAllList = () => {
  return axios.request({
    url: '/admin-service/role/all'
  })
}

/**
 * 根据角色id查询已授权的权限列表
 * @param roleId
 * @returns {*|AxiosPromise<any>|ClientRequest|void|ClientHttp2Stream}
 */
export const getAuthPowerListByRoleId = (roleId) => {
  return axios.request({
    url: '/admin-service/role/getAuthPowerListByRoleId',
    params: {
      roleId: roleId
    }
  })
}


/**
 * 保存授权权限
 * @param rolePowerDTO
 * @returns {*|AxiosPromise<any>|ClientRequest|void|ClientHttp2Stream}
 */
export const saveAuthPower = (rolePowerDTO) => {
  return axios.request({
    url: '/admin-service/role/saveAuthPower',
    method: 'post',
    data: rolePowerDTO
  })
};
