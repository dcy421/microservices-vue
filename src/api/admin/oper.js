import axios from '@/libs/api.request'

/**
 * 添加用户
 * @param oper
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const addOper = (oper) => {
  return axios.request({
    url: '/admin-service/oper/save',
    method: 'post',
    data: oper
  })
}

/**
 * 修改用户
 * @param oper
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const updateOper = (oper) => {
  return axios.request({
    url: '/admin-service/oper/update',
    method: 'post',
    data: oper
  })
}

/**
 * 删除用户
 * @param operId
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteOperById = (operId) => {
  return axios.request({
    url: '/admin-service/oper/delete',
    method: 'post',
    params: {
      id: operId
    }
  })
}

/**
 * 批量删除用户
 * @param operIds
 * @returns {ClientHttp2Stream | * | AxiosPromise<any> | ClientRequest | void}
 */
export const deleteBatchOperById = (operIds) => {
  return axios.request({
    url: '/admin-service/oper/deleteBatch',
    method: 'post',
    data: operIds
  })
}
