# microservices-vue

#### 介绍
spring-cloud 后台管理，对应dcy-microservices-platform项目地址
[https://gitee.com/dcy421/dcy-microservices-platform](https://gitee.com/dcy421/dcy-microservices-platform)

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

一、构建docker镜像
```
cd microservices-vue
docker build -t mic-vue -f docker/Dockerfile .
```
二、运行容器
```
docker run --name mic-vue -p 80:80 -d mic-vue
```
三、访问
http://localhost

用户名密码 admin、123456
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
